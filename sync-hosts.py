#!/usr/bin/env python

import subprocess
import threading
import time

print "sync-hosts.py running..."


def getRobots():
    proc = subprocess.Popen(['nmap', '-sP', '192.168.1.0/24'], stdout=subprocess.PIPE)
    results = []
    for line in proc.stdout:
        str = line.rstrip()
        if str.startswith('Nmap scan report for'):
            result = str.replace('Nmap scan report for ', '')
            if not result ==  "192.168.1.100" and not result == "192.168.1.1":
                #print "result=" + result
                results.append(result)
    return results

def start_shell(command):
    t = threading.Thread(target=run_shell, kwargs={"command":command})
    t.daemon = True
    t.start()

def run_shell(command):
    print "running=" + command
    proc = subprocess.Popen(command, universal_newlines=True, shell=True)

def sync_robots():
    robots = getRobots()
    print robots
    for bot in robots:
        rsync_command = "rsync -avi /home/ajc/fearing-workspace/deploy/ linaro@{0}:/home/linaro/deploy".format(bot)
        command = "urxvt -e sh -c 'echo {0}; {0}; echo done!;bash' ".format(rsync_command)
        print command
        run_shell(command)
    #run_shell(mount_cmd)

sync_robots()
