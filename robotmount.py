#!/usr/bin/env python

import subprocess
import threading
import time

print "robotmount.sh running..."


def getRobots():
    proc = subprocess.Popen(['nmap', '-sP', '192.168.1.0/24'], stdout=subprocess.PIPE)
    results = []
    for line in proc.stdout:
        str = line.rstrip()
        if str.startswith('Nmap scan report for'):
            result = str.replace('Nmap scan report for ', '')
            if not result ==  "192.168.1.100" and not result == "192.168.1.1":
                #print "result=" + result
                results.append(result)
    return results

def start_shell(command):
    t = threading.Thread(target=run_shell, kwargs={"command":command})
    t.daemon = True
    t.start()

def run_shell(command):
    print "running=" + command
    proc = subprocess.Popen(command, universal_newlines=True, shell=True)

def open_robots():
    robots = getRobots()
    print robots
    for bot in robots:
        mkdir_cmd = "mkdir -p ~/fearing-workspace/robots/{0}".format(bot)
        run_shell(mkdir_cmd)
        mount_cmd = "sshfs linaro@{0}:/home/linaro ~/fearing-workspace/robots/{0}".format(bot)
        #print command
        run_shell(mount_cmd)

open_robots()
