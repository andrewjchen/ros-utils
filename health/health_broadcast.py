import lcm
import time
import datetime
import psutil

from fearing import odroid_health

def uptime():
	with open('/proc/uptime', 'r') as f:
		uptime_seconds = float(f.readline().split()[0])
#$		uptime_string = str(timedelta(seconds = uptime_seconds))

	return uptime_seconds
def load_average():
	la = []
	with open('/proc/loadavg', 'r') as f:
		nums = f.readline().split()
		for i in range(3):
			la.append(float(nums[i]))

	return la

def cpufreq():
	freqs = []
	with open ('/proc/cpuinfo', 'r') as f:
		for line in f:
			if line.startswith('cpu MHz'):
				freqs.append(float(line.split()[3]))
				
	while len(freqs) < 4:
		freqs.append(-1)

	return freqs

def cpu_use():

	retu = psutil.cpu_percent(interval=.25, percpu=True)
	while len(retu) < 4:
		retu.append(-1)
	return retu


lc = lcm.LCM('udpm://239.255.76.67:7667?ttl=1')

while 1:
	msg = odroid_health()

	now = datetime.datetime.utcnow()
	seconds = (now - datetime.datetime(1970, 1, 1)).total_seconds()
	msg.unix_time = seconds
	msg.uptime = uptime()
	msg.load_average = load_average()
	msg.cpu_use = cpu_use()

	mem = psutil.virtual_memory().active / (10**6)
	msg.memory_use = mem
	msg.cpu_freq = cpufreq()

	lc.publish("b100/health", msg.encode())
