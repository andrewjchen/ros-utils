#!/usr/bin/env python

import subprocess

print "Hello, world!"

def writeAddressToFile(file, iface):
    f = open(file, 'a')
    addr = getAddr(iface)
    f.write("export ROS_IP={0}\n".format(addr))
    f.write("export ROS_MASTER_URI=http://{0}:11311\n".format(addr))
    f.write('\n')


def getAddr(iface):
    #print "starting"
    proc = subprocess.Popen(['ifconfig'], stdout=subprocess.PIPE)
    foundinterface = False
    for line in proc.stdout:
        str = line.rstrip()
        if str.find('Link') > 0 and str.startswith(iface):
            foundinterface = True
        if foundinterface:
            if str.find('inet addr') > 0:
                #print "test:", str
                #print str.find('inet addr')
                parts = str.split(' ')
                #print parts
                for part in parts:
                    if part.startswith('addr:'):
                        address = part.replace('addr:', '')
                        return address

print getAddr('ra0')
writeAddressToFile('/home/linaro/.bashrc', 'ra0')
