#!/bin/bash

screen -S _roscore -d -m ~/ros-utils/roscore.sh
screen -S _master_discovery -d -m ~/ros-utils/master-discovery.sh
screen -S _master_discovery -d -m ~/ros-utils/master-sync.sh